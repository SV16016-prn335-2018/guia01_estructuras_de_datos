#include <stdio.h>
#include <stdlib.h>

int main(void) {
int columnas, filas;
  printf("Cuantas columnas quieres ");
  scanf("%d", &columnas);
  printf("Cuantas filas quieres ");
  scanf("%d", &filas);
    int matriz[filas][columnas];
    int horizontal[filas];
    int vertical[columnas];
    int fila, columna;
    int suma = 0;
    int i;
    float sumadiagonal;

    
    for (fila = 0; fila < filas; fila++) {
        for (columna = 0; columna < columnas; columna++) {
            printf("Introduce un numero para la posicion %d,%d: ", fila, columna);
            scanf("%d", &matriz[fila][columna]);
        }
    }


    for (fila = 0; fila < filas; fila++) {
        suma = 0;
        for (columna = 0; columna < columnas; columna++) {
            suma += matriz[fila][columna];
        }
        vertical[fila] = suma;
    }

  printf("Sumatoria de las columnas:\n");
    for (i = 0; i < filas; i++) {
        printf("%d\n", vertical[i]);
    }

  
    for (columna = 0; columna < columnas; columna++) {
        suma = 0;
        for (fila = 0; fila < filas; fila++) {
            suma += matriz[fila][columna];
        }
        horizontal[columna] = suma;
    }
  printf("Sumatoria de las filas:\n");
    for (i = 0; i < columnas; i++) {
        printf("%d\n", horizontal[i]);
    }
  
  for ( fila= 0 ; fila< filas ; fila++ ){
        sumadiagonal += matriz [ fila ][ filas - fila - 1 ] ;
  }
  printf("Sumatoria de la diagonal: %f\n",sumadiagonal);
}

 
